import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { MatDialog } from '@angular/material/dialog';
import { SocketClientChatService } from 'src/app/services/socket-client-chat.service';
import { SocketClientUserService } from 'src/app/services/socket-client-user.service';
import { MessageDialogComponent } from './popups/message-dialog/message-dialog.component';
import { CommandDialogComponent } from './popups/command-dialog/command-dialog.component';
import { MessageInputDialogComponent } from './popups/message-input-dialog/message-input-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  @ViewChild('changeLog', { static: false })
  private changeLogRef: any;

  private socketSubscription: Subscription;

  constructor(private router: Router, public userService: SocketClientUserService,
              public chatService: SocketClientChatService,
              public dialogService: MatDialog) {
    // Initialize client socket
    this.chatService.initializeSocket();
  }

  public ngOnInit() {
    // Subscription to receive custom events from client socket
    this.socketSubscription = this.chatService.receiveEvent()
      .subscribe(($data: any) => {
        if ($data && $data.type) {
          if ($data.type === 'message') {
            // Open message popup
            this.openMessageDialog({
              message: $data.data.message
            });
            this.chatService.log('bot', $data.data.message);
          } else if ($data.type === 'command') {
            if ($data.data.command.type === 'complete' && this.chatService.selectedOptions.length !== 3) {
              // 'complete' confirmation dialog will be shown only after other 3 widgets  
              this.pokeBot();
            } else if (this.chatService.selectedOptions.indexOf($data.data.command.type) === -1) {
              /* Each widget should be provided to the user only once, unless he wishes to continue.
                Hence, each time a widget is displayed, the corresponding key is pushed to an array
                in chatService */
              this.chatService.selectedOptions.push($data.data.command.type);
              this.openCommandDialog($data.data.command);
            } else {
              /* Since server socket sends responses in random, if an already used type
                 of command comes from server, skip it and request again */
              this.pokeBot();
            }
          } else if ($data.type === 'logout') {
            // Redirect to login page
            this.router.navigate(['/login']);
          } else if ($data.type === 'scrollToBottom') {
            if (this.changeLogRef && this.changeLogRef.nativeElement) {
              // To scroll to bottom each time a new log is pushed
              setTimeout(() => {
                this.changeLogRef.nativeElement.scrollTop
                  = this.changeLogRef.nativeElement.scrollHeight;
              }, 100);
            }
          }
        }
      });
  }

  // Method to open dialog for entering user input
  public openMessageInputDialog() {
    this.dialogService.open(MessageInputDialogComponent, {
      width: '400px',
      data: {  }
    });
  }

  // Method to open message dialog
  private openMessageDialog(inputData: any) {
    this.dialogService.open(MessageDialogComponent, {
      width: '350px',
      data: { message: inputData.message }
    });
  }

  // Method to open the interactive widget dialog
  private openCommandDialog(inputData: any) {
    let dialogWidth: number;
    switch (inputData.type) {
      case 'complete': {
        this.chatService.log('bot', 'Do you want to continue ?');
        dialogWidth = 400;
        break;
      }
      case 'rate': {
        this.chatService.log('bot', 'Please rate your experience');
        dialogWidth = 400;
        break;
      }
      case 'date': {
        this.chatService.log('bot', 'Please select a date');
        dialogWidth = 600;
        break;
      }
      case 'map': {
        this.chatService.log('bot', 'Please confirm your location');
        dialogWidth = 450;
        break;
      }
      default: break;
    }
    this.dialogService.open(CommandDialogComponent, {
      width: dialogWidth + 'px',
      data: inputData
    });
  }

  // Method to request 'Command' from server
  public pokeBot() {
    this.chatService.sendData('command', {});
  }

  // Lifecycle method to clear resources on destroying the component
  public ngOnDestroy() {
    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }
    this.chatService.disconnectSocket();
  }

}
