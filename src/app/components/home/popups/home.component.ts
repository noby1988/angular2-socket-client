import { Subscription } from 'rxjs/internal/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SocketClientChatService } from 'src/app/services/socket-client-chat.service';
import { SocketClientUserService } from 'src/app/services/socket-client-user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private socketSubscription: Subscription;
  public chatMode: number;
  public pingText: string;

  constructor(public userService: SocketClientUserService,
              private chatService: SocketClientChatService) {
    this.chatMode = -1;
    this.chatService.initializeSocket();
  }

  public ngOnInit() {
    this.socketSubscription = this.chatService.receiveEvent()
      .subscribe(($data: any) => {
        if ($data && $data.type) {
          if ($data.type === 'message') {
            console.log('message');
            console.log($data.data.message);
          } else if ($data.type === 'command') {
            if ($data.data) {
              console.log('command');
              console.log($data.data.type);
              console.log($data.data.data);
            }
          }
        }
      });
  }

  public pingBot() {
    this.chatService.sendData('message', {
      author: this.userService.getUsername(),
      message: this.pingText
    });
    this.chatMode = -1;
  }

  public pokeBot() {
    this.chatService.sendData('command', {});
  }

  public ngOnDestroy() {
    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }
    this.chatService.disconnectSocket();
  }

}
