import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { LoggedInAuthguard } from './services/logged-in-authguard.service';

const routes: Routes = [{ path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      // Authguard is defined for home component. If not logged in, user will be directed to login page
      { path: 'home', component: HomeComponent, canActivate: [LoggedInAuthguard] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
