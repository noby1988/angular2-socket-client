import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SocketClientUserService {

  private username: string;

  constructor() {
    this.username = '';
  }

  public setUsername(username: string) {
    this.username = username;
  }

  public getUsername(): string {
    return this.username;
  }

}
